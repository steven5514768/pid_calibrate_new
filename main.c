
#include "embARC.h"
#include "embARC_debug.h"
#include "mpu9250.h"
#include "stdio.h"
#include "header.h"

#include "pid_calibrate.h"


int main(void)
{
    float goal_pitch = 0;
    float goal_roll = 0;
    float goal_yaw = 0;
    int initial_speed = 0;
    arduino_pin_init();
    pwm_set(0,720);
    pwm_set(1,720);
    pwm_set(2,720);
    pwm_set(3,720);
    for (int i=0; i<100; i++)
    {
        board_delay_ms(100, 1);
    } 
    
    loiter_t loiter_parameter;
    loiter_initial_parameter(&loiter_parameter);
    loiter_set_parameter(&loiter_parameter, goal_pitch, goal_roll, goal_yaw);


    initial_speed = 1775;
    /*
    for (int i=0; i<loopTime_1; i++)
    {
        loiter(&loiter_parameter, i, initial_speed);
        //notice: The function will use Timer1 to calibrate the yaw, so don't use Timer1 when using this function. 
        board_delay_ms(1000.0/loopTime_1, 1);
    }
    */
    initial_speed = 1775;
    for (int i=0; i<loopTime; i++)
    {
        loiter(&loiter_parameter, i, initial_speed);
        //notice: The function will use Timer1 to calibrate the yaw, so don't use Timer1 when using this function. 
        board_delay_ms(20000.0/loopTime, 1);
    }
/*    initial_speed = 1850;
    for (int i=0; i<100; i++)
    {
        loiter(&loiter_parameter, i, initial_speed);
        //notice: The function will use Timer1 to calibrate the yaw, so don't use Timer1 when using this function. 
        board_delay_ms(10, 1); 
    }*/
    // printf("t : %d\r\n", loiter_parameter.t);
    //initial_speed = 1950;
    /*
    for (int i=1; i<500; i++)
    {
        loiter(&loiter_parameter, i, initial_speed);
        //notice: The function will use Timer1 to calibrate the yaw, so don't use Timer1 when using this function. 
        board_delay_ms(10, 1);
    }

    //initial_speed = 41;
    for (int i=1; i<300; i++)
    {
        loiter(&loiter_parameter, i, initial_speed);
        //notice: The function will use Timer1 to calibrate the yaw, so don't use Timer1 when using this function. 
        board_delay_ms(10, 1); 
    }
    //initial_speed = 41;
    for (int i=1; i<300; i++)
    {
        loiter(&loiter_parameter, i, initial_speed);
        //notice: The function will use Timer1 to calibrate the yaw, so don't use Timer1 when using this function. 
        board_delay_ms(10, 1);
    }

	for (int k=0; k<loiter_parameter.t; k++)
	{
		printf("%d\n", loiter_parameter.i_1[k]);
	
	}
    
    
    //pwm_set(0,25);
    /*for (int i=0; i<50;i++)
    {
        board_delay_ms(100, 1);
    }*/
/*    FILE *pFileBefore;
    pFileBefore = fopen("Before.txt","wb");
    printf("start");
    if( NULL == pFileBefore)

    {

        printf(" failure");

        return 1;

    }
*/
    pwm_set(0,0);
    pwm_set(1,0);
    pwm_set(2,0);
    pwm_set(3,0);

    char pitch_buffer[7];
    char d_pitch_buffer[7];
    char roll_buffer[7];
    char d_roll_buffer[7];
    float tmp = 0.05;
    int i = 0;
    for (int i = 0; i < 4000; i++) {
/*      sprintf(d_pitch_buffer, "%6f", raw_data[i].pitch_dump);
        sprintf(pitch_buffer, "%6f", raw_data[i].pitch_pid);
        sprintf(d_roll_buffer, "%6f", raw_data[i].roll_dump);
        sprintf(roll_buffer, "%6f", raw_data[i].roll_pid);
        fwrite( d_pitch_buffer, 1, sizeof(double) + 1, pFileBefore);
        fwrite( pitch_buffer, 1, sizeof(double) + 1, pFileBefore);
        fwrite( d_roll_buffer, 1, sizeof(double) + 1, pFileBefore);
        fwrite( roll_buffer, 1, sizeof(double) + 1, pFileBefore);*/
        printf("%6f;", raw_data[i].pitch_dump);
        printf("%6f;", raw_data[i].pitch_pid);
        printf("%6f;", raw_data[i].roll_dump);
        printf("%6f;", raw_data[i].roll_pid);
        printf("\r\n");
    }
    
    //fclose(pFileBefore);

    //printf("finish!!");
    

    return E_OK;
}

