#include "embARC.h"
#include "embARC_debug.h"
#include "mpu9250.h"
#include "stdio.h"
#include "header.h"
#include "pid_calibrate.h"
#include <math.h>

#if defined(BOARD_IOTDK) || defined(BOARD_EMSDP)
#define MPU9250_IIC_ID DFSS_IIC_0_ID
#else
#define MPU9250_IIC_ID 0
#endif
///////////////////////////////////////////////////////////////////////

#define calibrate 0.54
#define pi 3.141593

void yaw_error_count_start(void)
{
    timer_int_clear(TIMER_1);
    timer_stop(TIMER_1);
	timer_start(TIMER_1, TIMER_CTRL_IE, BOARD_CPU_CLOCK*1000);
}





/*uint64_t real_timer_ms(void)
{
	uint64_t val;
	uint64_t out;
	timer_current(TIMER_1, &val);
	val /= 144000;
    out = val;
	return out;
}
*/
/*uint64_t real_timer_s(int TIMER)
{
	uint64_t val;
	timer_current(TIMER, &val);
	val /= 144000000;

	return val;
}
*/
// yaw error calibrate

void yaw_calibrate(float *error, uint64_t *time, uint64_t *pre) 
{
	if (*pre != *time) {
			*error += calibrate;
			*pre = *time;
	}

	return;
}

////////////////////////////////////////////////////////////
//#define calibrate 0.535

static MPU9250_DEFINE(mpu9250_sensor, MPU9250_IIC_ID, MPU9250_IIC_ADDRESS);

float error = 0;
uint64_t pre_time = 0;		// record golbal previous time
uint64_t time_1 = 0;		// record golbal time


uint64_t real_timer_s(int TIMER)
{
	uint64_t val;
	timer_current(TIMER, &val);
	val /= 144000000;

	return val;
}

void loiter_initial_parameter(loiter_t *loit)
{
   	loit->pid_ret=0;				// 1: set unsuccessful; 0: set successful
	//loiter_parameter->mpu9250_data = { 0 };
	loit->kp_p = 5.5;
	loit->ki_p = 1.2;
	loit->kd_p = 4.3;

	loit->kp_r = 4.25; //3;
	loit->ki_r = 1.2; //3.5;
	loit->kd_r = 3.3; //2.4;

	loit->kp_y = 2.5;
	loit->ki_y = 3.5;
	loit->kd_y = 1.0;

	loit->integral_limit = 150;
	loit->output_limit = 300;
	
	loit->pitch_pid = 0;
	loit->roll_pid = 0;
	loit->yaw_pid = 0;

	loit->speed_pitch = 0;
	loit->speed_roll = 0;
	loit->speed_yaw = 0;

	loit->pre_error = 0;

	loit->pin_motor1 = 0; //clockwise
	loit->pin_motor2 = 1; //counterclockwise
	loit->pin_motor3 = 2; //counterclockwise
	loit->pin_motor4 = 3; //clockwise
	/*
    * 板子左右為pitch(板子原始方向)，yaw順時針轉為負，安裝時請以字母A朝前方為標準，
	* 左上為1號馬達，右上為2號，左下3號，右下4號(A朝前方為標準)
    */
	loit->speed_motor1 = 0;
	loit->speed_motor2 = 0;
	loit->speed_motor3 = 0;
	loit->speed_motor4 = 0;
    
	loit->calibrate_max_speed = 15; //馬達引擎動力差異上限 

	//loit->motor_initial_speed = initial_speed; //維持高度速度，待校正

	float tmp = 20.0/loopTime;
	pid_init(&loit->pid_p, PID_MODE_DERIVATIV_CALC, tmp);
	pid_init(&loit->pid_r, PID_MODE_DERIVATIV_CALC, tmp);
	pid_init(&loit->pid_y, PID_MODE_DERIVATIV_CALC, tmp);

	loit->pid_ret = pid_set_parameters(&(loit->pid_p), loit->kp_p, loit->ki_p, loit->kd_p, loit->integral_limit, loit->output_limit);

	if (loit->pid_ret)
	{
		EMBARC_PRINTF("Pid setting Failed");
	}
	else
	{
		EMBARC_PRINTF("Pid setting successful");
	}
	loit->pid_ret = pid_set_parameters(&loit->pid_r, loit->kp_r, loit->ki_r, loit->kd_r, loit->integral_limit, loit->output_limit);
	loit->pid_ret = pid_set_parameters(&loit->pid_y, loit->kp_y, loit->ki_y, loit->kd_y, loit->integral_limit, loit->output_limit);
	
	EMBARC_PRINTF("\r\n\r\n\r\n");
	EMBARC_PRINTF("Pitch and Roll calibrating is about to start in 3 second\n");

	for (int i=0; i<3; i++)
	{
		printf("%d\n", 3-i);
        for (int k=0; k<10; k++)
		{
          board_delay_ms(100,1);
		}	
	}


	mpu9250_sensor_init(mpu9250_sensor);
	for (int i=0; i<6; i++)
	{
		printf("Calibrating! Don't move the drone :%d s\n",3-i);
        for (int k=0; k<10; k++)
		{
          board_delay_ms(100,1);
		}	
	}

	MPU9250_DATA mpu9250_data = { 0 };
    
	EMBARC_PRINTF("\r\n\r\n\r\n");
	int jump=0;
	for(int i=0; jump==0;i++) {
		mpu9250_sensor_read(mpu9250_sensor, &mpu9250_data);

		char datap[10];
		char datar[10];
		char datay[10];
		 
		sprintf(datap, "%06.1f", mpu9250_data.pitch);
		sprintf(datar, "%06.1f", mpu9250_data.roll);
		sprintf(datay, "%06.1f", mpu9250_data.yaw);

		//printf("dmp:  pitch=%.3f,  roll=%.3f,  yaw=%.3f \r\n", mpu9250_data.pitch, mpu9250_data.roll, mpu9250_data.yaw);
		printf("dmp:  pitch=%s,  roll=%s,  yaw=%s \r\n", datap, datar, datay);
		board_delay_ms(10, 1);
		//EMBARC_PRINTF("\x1b[2k\x1b\x45");
        if(mpu9250_data.pitch>10||mpu9250_data.pitch<-10||mpu9250_data.roll>10||mpu9250_data.roll<-10)
		{
			jump=0;
		}
		else if(i>1)
		{
            jump=1;			
		}
		EMBARC_PRINTF("\x1b[2k\x1b\x45");
		
	}
	

    /*
    while( loit->mpu9250_data.pitch>5 || loit->mpu9250_data.roll>5)
	{
		 printf("pitch_error=%f",  loit->mpu9250_data.pitch);
	     printf("roll_error=%f",loit->mpu9250_data.roll);
	  for (int i=0; i<3; i++)
	  {
		printf("Calibrating! Don't move the drone :%d s\n",3-i);
        for (int k=0; k<10; k++)
		{
          board_delay_ms(100,1);
		}	
	  }	
	  mpu9250_sensor_read(mpu9250_sensor, &loit->mpu9250_data);	
	}
    */
    
	loit->goal_pitch_error = mpu9250_data.pitch;
	loit->goal_roll_error = mpu9250_data.roll;
	//loit->pre_error = -loit->mpu9250_data.yaw;
/*	printf("yaw= %f\r\n", -loit->mpu9250_data.yaw);
    printf("pitch_error=%f", loit->goal_pitch_error);
	printf("roll_error=%f", loit->goal_roll_error);
*/
	
	// mpu9250_sensor_read(mpu9250_sensor, &loit->mpu9250_data);
	
    yaw_error_count_start();	//clear and restart the clock
	arduino_pin_init();
}



void loiter_set_parameter(loiter_t *loit, float goal_pitch_in, float goal_roll_in, float goal_yaw_in)
{
    loit->goal_pitch = goal_pitch_in; //goal_pitch_in+loit->goal_pitch_error;
	loit->goal_roll = goal_roll_in;//goal_roll_in+loit->goal_roll_error;
	loit->goal_yaw = goal_yaw_in;
}


int data_idx = 0;

void loiter(loiter_t *loit,int i,int initial_speed)
{
		//yaw_calibrate
		time_1 = real_timer_s(TIMER_1);		

	    yaw_calibrate(&error, &time_1, &pre_time);
        /*
		if (error>20)
		{
           mpu9250_sensor_init(mpu9250_sensor);
		   error=0;
		}
        //yaw_calibrate
        */  
		mpu9250_sensor_read(mpu9250_sensor, &loit->mpu9250_data);
        if (error>15)
		{
			loit->pre_error = -loit->mpu9250_data.yaw;
			error = 0;
		}
		else if(i==0)
		{
			loit->pre_error = -loit->mpu9250_data.yaw;
			error = 0;			
		}
		
//		printf("pre: %f error:%f yaw:%f\r\n", loit->pre_error, error, loit->mpu9250_data.yaw);
        float total_yaw_error = loit->pre_error+error+loit->mpu9250_data.yaw;
		if (total_yaw_error>180)
		{
			total_yaw_error = total_yaw_error-360;
		}
		
	    /*此兩行會造成程式嚴重問題，待查原因
		goal_pitch=goal_pitch+loit->goal_pitch_error;
		goal_roll=goal_roll+loit->goal_roll_error;
        */

      

		//printf("error=%f",error);
		loit->pitch_pid = pid_calculate(&loit->pid_p, loit->goal_pitch, loit->mpu9250_data.pitch, 0, 0.01);
		loit->pitch_pid/=cos(loit->mpu9250_data.pitch/180*pi);
		if(loit->pitch_pid>500)
		{
			loit->pitch_pid=500;
		}
		loit->roll_pid = pid_calculate(&loit->pid_r, loit->goal_roll, loit->mpu9250_data.roll, 0, 0.01);
		loit->roll_pid/=cos(loit->mpu9250_data.roll/180*pi);
		if(loit->roll_pid>500)
		{
			loit->roll_pid=500;
		}
		loit->yaw_pid = yaw_pid_calculate(&loit->pid_y, loit->goal_yaw, total_yaw_error, 0, 0.01);
		
		if (loit->mpu9250_data.pitch < 5 && loit->mpu9250_data.pitch > -5) {
			pid_reset_integral(&loit->pid_p);
		}
		if (loit->mpu9250_data.roll < 3 && loit->mpu9250_data.roll > -3) {
			pid_reset_integral(&loit->pid_r);
		}
		if (loit->mpu9250_data.yaw < 5 && loit->mpu9250_data.yaw > -5) {
			pid_reset_integral(&loit->pid_y);
		}		
        

       /*
		printf("i=%d\n", i);
		
		printf("dmp:  pitch=%.3f,  roll=%.3f,  yaw=%.3f \r\n", loit->mpu9250_data.pitch, loit->mpu9250_data.roll, total_yaw_error);
		printf("goal:  pitch=%.3f,  roll=%.3f,  yaw=%.3f \r\n", loit->goal_pitch, loit->goal_roll, loit->goal_yaw);
		printf("PID for pitch: %f\r\n", loit->pitch_pid);
		printf("PID for roll: %f\r\n", loit->roll_pid);		
		printf("PID for yaw: %f\r\n", loit->yaw_pid);
        */
		if (i%5==0) {
			raw_data[data_idx].pitch_dump = loit->mpu9250_data.pitch;
			raw_data[data_idx].roll_dump = loit->mpu9250_data.roll;
			raw_data[data_idx].pitch_pid = loit->pitch_pid;
			raw_data[data_idx].roll_pid = loit->roll_pid;
			data_idx++;
		}
		/*
		if ((loit->mpu9250_data.pitch>10) || (loit->mpu9250_data.pitch<-10))
		{
		   if(loit->t<10)
		   {
			  loit->i_1[loit->t]=i;
		      loit->t++; 
		   }
           else{
			   printf("Error: invalid index!");
		   }
		}
		else if ((loit->mpu9250_data.roll>10) || (loit->mpu9250_data.roll<-10))
		{
           if(loit->t<10)
		   {
			  loit->i_1[loit->t]=i;
		      loit->t++; 
		   }
           else 
		   {
			   printf("Error: invalid index!");
		   }
		}
		else if ((total_yaw_error)>10 || (total_yaw_error)<-10)
		{
		   if(loit->t<10)
		   {
			  loit->i_1[loit->t]=i;
		      loit->t++; 
		   }
           else
		   {
			   printf("Error: invalid index!");
		   }
		}
        */
		loit->speed_pitch =0;// loit->pitch_pid; // * loit->calibrate_max_speed / loit->output_limit;
		loit->speed_roll = loit->roll_pid; //* loit->calibrate_max_speed / loit->output_limit;
		loit->speed_yaw = 0;//cdloit->yaw_pid; //* loit->calibrate_max_speed / loit->output_limit;

		loit->speed_motor1 = initial_speed + loit->speed_pitch - loit->speed_roll; // + loit->speed_yaw;
		loit->speed_motor2 = initial_speed + loit->speed_pitch + loit->speed_roll+10; // - loit->speed_yaw;
		loit->speed_motor3 = initial_speed - loit->speed_pitch - loit->speed_roll+34; // -loit->speed_yaw;
		loit->speed_motor4 = initial_speed - loit->speed_pitch + loit->speed_roll+44; // + loit->speed_yaw;
        
		pwm_set(loit->pin_motor1, loit->speed_motor1);
		pwm_set(loit->pin_motor2, loit->speed_motor2);
		pwm_set(loit->pin_motor3, loit->speed_motor3);
		pwm_set(loit->pin_motor4, loit->speed_motor4);		
	
		//board_delay_ms(10, 1); //原為100，跟友廷確認
        
		//EMBARC_PRINTF("\x1b[2k\x1b\x45");
}