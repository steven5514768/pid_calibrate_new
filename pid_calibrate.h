#ifndef _pid_calibrate_H_
#define _pid_calibrate_H_


#if defined(BOARD_IOTDK) || defined(BOARD_EMSDP)
#define MPU9250_IIC_ID DFSS_IIC_0_ID
#else
#define MPU9250_IIC_ID 0
#endif

#define loopTime_1 1000
#define loopTime 20000

typedef struct {
	int pid_ret;				// 1: set unsuccessful; 0: set successful
	PID_t pid_p;
	PID_t pid_r;
	PID_t pid_y;
	MPU9250_DATA mpu9250_data;
	float kp_p;
	float ki_p;
	float kd_p;
	float kp_r;
	float ki_r;
	float kd_r;
	float kp_y;
	float ki_y;
	float kd_y;
	float integral_limit;
	float output_limit;
	
	float pitch_pid;
	float roll_pid;
	float yaw_pid;

	float speed_pitch;
	float speed_roll;
	float speed_yaw;

	int pin_motor1;//counterclockwise
	int pin_motor2;//clockwise
	int pin_motor3;//clockwise
	int pin_motor4;//counterclockwise

	float speed_motor1;
	float speed_motor2;
	float speed_motor3;
	float speed_motor4;

	float goal_pitch_error;//pitch starting point
	float goal_roll_error;

	float goal_pitch;
	float goal_roll;
	float goal_yaw;
    
	int calibrate_max_speed;

	int motor_initial_speed;

	float pre_error;

	int i_1[10];
	int t;
} loiter_t;
void loiter(loiter_t *loit, int i,int initial_speed);
void loiter_initial_parameter(loiter_t *loit);
void loiter_set_parameter(loiter_t *loit, float goal_pitch_in, float goal_roll_in, float goal_yaw_in);

typedef struct pid_data {
	float roll_dump;
	float pitch_dump;
	float roll_pid;
	float pitch_pid;
} pid_data;

pid_data raw_data[4000];


#endif
